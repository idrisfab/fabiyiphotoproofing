<?php

namespace App\Http\Controllers;

use App\FabEvent;
use Illuminate\Http\Request;

class FabEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FabEvent  $fabEvent
     * @return \Illuminate\Http\Response
     */
    public function show(FabEvent $fabEvent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FabEvent  $fabEvent
     * @return \Illuminate\Http\Response
     */
    public function edit(FabEvent $fabEvent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FabEvent  $fabEvent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FabEvent $fabEvent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FabEvent  $fabEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy(FabEvent $fabEvent)
    {
        //
    }
}
