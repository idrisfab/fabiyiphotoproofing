<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFabEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fab_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('EventName');
            $table->date('EventDate')->default(Carbon::now());
            $table->string('EventThumbnail')->default('\images\no-image-icon.png');
            $table->integer('UserId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fab_events');
    }
}
